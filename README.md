## STATUS - Partial Completion

* Completed the design & convertion
* Consumed the Duckduckgo API
* Time Used - 7-8Hours approx.

**Libraries Used**

1. HTML5 Boilerplate
2. Bootstrap
3. JQuery
4. LESS for CSS compilation
5. HandlebarJS

**Dev Enviroment**

1. Sublime Text
2. NodeJS for task management
3. Bitbucket for repo management
4. Mavericks OSX
5. Chrome v38

**Known Issues**

1. API issue with the Results key
2. Browser issue with Safari/Firfox
3. Unsliced Images
4. Not tested in IE
5. AJAX method needs refactoring

**Steps to run the app:**

1. ```npm install```
2. ```bower install```
3. ```grunt```

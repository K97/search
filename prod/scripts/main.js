$(function() {
  var doc = $(document),
      searchForm = $('.search-form');

  getTemplateAjax('templates/search.hbs',searchForm);
  homePageTemplate();
  doc.ajaxComplete(function(){
    searchForm.submit(searchMain);
    //Small hack to reload view
    $('.homePageLogo').click(function(){
      window.location.reload();
    })
  });//ajaxComplete


});//DOM

/*------------------------------------------------------------------
[SEARCH MAIN FUNCATION]
-------------------------------------------------------------------*/

function searchMain(event){
  event.preventDefault();
  searchInput = $('.search-box').filter(':visible:first').val();
  commObj(searchInput);
  return false;
}//searchForm

function homePageTemplate(){
  $('#homePage').show();
  $('#searchResultsPage').hide();
}

function searchResultsTemplate(){
  $('#homePage').hide();
  $('#searchResultsPage').show();

}
/*------------------------------------------------------------------
[BIND RESULTS]
-------------------------------------------------------------------*/
function BindSearchResults(data){
  searchResultsTemplate();
  var template = Handlebars.compile($('#results-template').html());
  Handlebars.registerPartial("RelatedTopics", $("#results-template").html());
  $('#resultsInfo').html(template(data));
    if(!data.RelatedTopics)
      $('#resultsTopic').hide();
}

/*------------------------------------------------------------------
[AJAX - Communication/Templates]
-------------------------------------------------------------------*/

function commObj(requestParam){
  if (requestParam) {
    console.log(requestParam);
    $('.search-loader').show();
    $.ajax({
      type: 'GET',
      url: 'https://api.duckduckgo.com/',
      data: {
        q: requestParam,
        format: 'json',
        pretty: 1
      },
      jsonpCallback: 'jsonp',
      dataType: 'jsonp'
    }).success(function(data) {
      $('.search-loader').hide();
      BindSearchResults(data);
    }).error(function(error) {
      alert(error)
    });
  }//if

}//commObj

function getTemplateAjax(path,target) {
  var source;
  var template;
  $.ajax({
    url: path, //ex. js/templates/mytemplate.handlebars
    cache: true,
    success: function(data){
      source    = data;
      template  = Handlebars.compile(source);
      target.html(template);
    }
  });
}//getTemplateAjax

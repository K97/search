var data = {
    "DefinitionSource": "",
    "Heading": "India",
    "ImageWidth": 0,
    "RelatedTopics": [
        {
            "Result": "<a href=\"https://duckduckgo.com/India\">India</a> A country in South Asia. It is the seventh-largest country by area, the second-most populous...",
            "Icon": {
                "URL": "https://duckduckgo.com/i/cef47a13.png",
                "Height": "",
                "Width": ""
            },
            "FirstURL": "https://duckduckgo.com/India",
            "Text": "India A country in South Asia. It is the seventh-largest country by area, the second-most populous..."
        },
        {
            "Result": "<a href=\"https://duckduckgo.com/India.Arie\">India.Arie</a>India.Arie is a Grammy Award-winning American singer-songwriter, musician, and record producer.",
            "Icon": {
                "URL": "https://duckduckgo.com/i/22507013.jpg",
                "Height": "",
                "Width": ""
            },
            "FirstURL": "https://duckduckgo.com/India.Arie",
            "Text": "India.ArieIndia.Arie is a Grammy Award-winning American singer-songwriter, musician, and record producer."
        },
        {
            "Topics": [
                {
                    "Result": "<a href=\"https://duckduckgo.com/Indies\">Indies</a>A term that has been used to describe the lands of South and South East Asia, occupying all of...",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/4e4e6f1f.jpg",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/Indies",
                    "Text": "IndiesA term that has been used to describe the lands of South and South East Asia, occupying all of..."
                }
            ],
            "Name": "Geography and culture"
        },
        {
            "Topics": [
                {
                    "Result": "<a href=\"https://duckduckgo.com/Bharata_Khanda\">Epic India</a>A term used in Hindu texts, including the Vedas, Mahabharata, Ramayana and the Puranic, for the...",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/54a6659a.jpg",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/Bharata_Khanda",
                    "Text": "Epic IndiaA term used in Hindu texts, including the Vedas, Mahabharata, Ramayana and the Puranic, for the..."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/Mahajanapada\">Mahajanapada</a>Mahājanapada refers to ancient Indian kingdoms that existed between the sixth and third centuries...",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/75f40fda.png",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/Mahajanapada",
                    "Text": "MahajanapadaMahājanapada refers to ancient Indian kingdoms that existed between the sixth and third centuries..."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/Hindustan\">Hindustan</a> A popular name for the northern/northwestern Indian subcontinent.",
                    "Icon": {
                        "URL": "",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/Hindustan",
                    "Text": "Hindustan A popular name for the northern/northwestern Indian subcontinent."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/British_Raj\">British Raj</a>The British rule in the Indian subcontinent between 1858 and 1947.",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/7291772d.png",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/British_Raj",
                    "Text": "British RajThe British rule in the Indian subcontinent between 1858 and 1947."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/India_(East_Syrian_Ecclesiastical_Province)\">India (East Syrian Ecclesiastical Province)</a>An ecclesiastical province of the Church of the East, at least nominally, from the seventh to the...",
                    "Icon": {
                        "URL": "",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/India_(East_Syrian_Ecclesiastical_Province)",
                    "Text": "India (East Syrian Ecclesiastical Province)An ecclesiastical province of the Church of the East, at least nominally, from the seventh to the..."
                }
            ],
            "Name": "History"
        },
        {
            "Topics": [
                {
                    "Result": "<a href=\"https://duckduckgo.com/La_India\">La India</a> A singer of salsa and house music. She has been nominated for both Grammy and Latin Grammy awards.",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/elmundodeindia.com.ico",
                        "Height": 16,
                        "Width": 16
                    },
                    "FirstURL": "https://duckduckgo.com/La_India",
                    "Text": "La India A singer of salsa and house music. She has been nominated for both Grammy and Latin Grammy awards."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/%C3%8Dndia_(Gal_Costa_album)\">Índia (Gal Costa album)</a>An 1973 studio album by the singer Gal Costa.",
                    "Icon": {
                        "URL": "",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/%C3%8Dndia_(Gal_Costa_album)",
                    "Text": "Índia (Gal Costa album)An 1973 studio album by the singer Gal Costa."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/India_(Vega_album)\">India (Vega album)</a> The first studio album by Spanish singer Vega, released on November 7, 2003 on Vale Music Spain.",
                    "Icon": {
                        "URL": "",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/India_(Vega_album)",
                    "Text": "India (Vega album) The first studio album by Spanish singer Vega, released on November 7, 2003 on Vale Music Spain."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/India_(Xandria_album)\">India (Xandria album)</a>The third studio album by German symphonic metal band Xandria.",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/190e165b.jpg",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/India_(Xandria_album)",
                    "Text": "India (Xandria album)The third studio album by German symphonic metal band Xandria."
                }
            ],
            "Name": "Music"
        },
        {
            "Topics": [
                {
                    "Result": "<a href=\"https://duckduckgo.com/India_(given_name)\">India (given name)</a>A feminine given name derived from the name of the country India, which takes its name from the...",
                    "Icon": {
                        "URL": "",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/India_(given_name)",
                    "Text": "India (given name)A feminine given name derived from the name of the country India, which takes its name from the..."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/India_(cat)\">India (cat)</a>A black cat owned by former U.S. President George W. Bush and First Lady Laura Bush.",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/60cd3bd6.jpg",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/India_(cat)",
                    "Text": "India (cat)A black cat owned by former U.S. President George W. Bush and First Lady Laura Bush."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/India_(battle_honour)\">India (Battle honour)</a>A battle honour awarded to the following regiments of the British Army for their service during...",
                    "Icon": {
                        "URL": "",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/India_(battle_honour)",
                    "Text": "India (Battle honour)A battle honour awarded to the following regiments of the British Army for their service during..."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/India-class_submarine\">India class submarine</a> A military submarine design of the Soviet Union.",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/89e9e229.jpg",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/India-class_submarine",
                    "Text": "India class submarine A military submarine design of the Soviet Union."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/Treasury_tag\">India tag</a> An item of stationery used to fasten sheets of paper together or to a folder.",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/7d906bb9.jpg",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/Treasury_tag",
                    "Text": "India tag An item of stationery used to fasten sheets of paper together or to a folder."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/In%C4%91ija\">Inđija</a>A town and a municipality located in Serbia.",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/53ddcc55.jpg",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/In%C4%91ija",
                    "Text": "InđijaA town and a municipality located in Serbia."
                }
            ],
            "Name": "Other uses"
        },
        {
            "Topics": [
                {
                    "Result": "<a href=\"https://duckduckgo.com/Pakistan\">Pakistan</a>A sovereign country in South Asia.",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/a8378626.png",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/Pakistan",
                    "Text": "PakistanA sovereign country in South Asia."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/Burma\">Burma</a> A sovereign state in Southeast Asia bordered by Bangladesh, India, China, Laos and Thailand.",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/3f4cc7bf.png",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/Burma",
                    "Text": "Burma A sovereign state in Southeast Asia bordered by Bangladesh, India, China, Laos and Thailand."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/Names_of_India\">Names of India</a>The name may refer to either the region of Greater India or to the contemporary Republic of India...",
                    "Icon": {
                        "URL": "https://duckduckgo.com/i/fe9ea1d7.jpg",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/Names_of_India",
                    "Text": "Names of IndiaThe name may refer to either the region of Greater India or to the contemporary Republic of India..."
                },
                {
                    "Result": "<a href=\"https://duckduckgo.com/d/Indian\">Indian Meanings</a>  See related meanings for the word 'Indian'.",
                    "Icon": {
                        "URL": "",
                        "Height": "",
                        "Width": ""
                    },
                    "FirstURL": "https://duckduckgo.com/d/Indian",
                    "Text": "Indian Meanings  See related meanings for the word 'Indian'."
                }
            ],
            "Name": "See also"
        }
    ],
    "Entity": "",
    "Type": "D",
    "Redirect": "",
    "DefinitionURL": "",
    "AbstractURL": "https://en.wikipedia.org/wiki/India_(disambiguation)",
    "Definition": "",
    "AbstractSource": "Wikipedia",
    "Infobox": "",
    "Image": "",
    "ImageIsLogo": 0,
    "Abstract": "",
    "AbstractText": "",
    "AnswerType": "",
    "ImageHeight": 0,
    "Results" : [
          {
             "Result" : "<a href=\"http://www.nps.gov/vafo/\"><b>Official site</b></a><a href=\"http://www.nps.gov/vafo/\"></a>",
             "Icon" : {
                "URL" : "https://duckduckgo.com/i/nps.gov.ico",
                "Height" : 16,
                "Width" : 16
             },
             "FirstURL" : "http://www.nps.gov/vafo/",
             "Text" : "Official site"
          },
          {
             "Result" : "<a href=\"http://www.nps.gov/vafo/\"><b>Goverment Website</b></a><a href=\"http://www.nps.gov/vafo/\"></a>",
             "Icon" : {
                "URL" : "https://duckduckgo.com/i/nps.gov.ico",
                "Height" : 16,
                "Width" : 16
             },
             "FirstURL" : "http://www.nps.gov/vafo/",
             "Text" : "Official site"
          },{
             "Result" : "<a href=\"http://www.nps.gov/vafo/\"><b>NPS Website site</b></a><a href=\"http://www.nps.gov/vafo/\"></a>",
             "Icon" : {
                "URL" : "https://duckduckgo.com/i/nps.gov.ico",
                "Height" : 16,
                "Width" : 16
             },
             "FirstURL" : "http://www.nps.gov/vafo/",
             "Text" : "Official site"
          },{
             "Result" : "<a href=\"http://www.nps.gov/vafo/\"><b>VAFO NPS Government Website</b></a><a href=\"http://www.nps.gov/vafo/\"></a>",
             "Icon" : {
                "URL" : "https://duckduckgo.com/i/nps.gov.ico",
                "Height" : 16,
                "Width" : 16
             },
             "FirstURL" : "http://www.nps.gov/vafo/",
             "Text" : "Another Official site"
          },
          {
             "Result" : "<a href=\"http://www.nps.gov/vafo/\"><b>Official site</b></a><a href=\"http://www.nps.gov/vafo/\"></a>",
             "Icon" : {
                "URL" : "https://duckduckgo.com/i/nps.gov.ico",
                "Height" : 16,
                "Width" : 16
             },
             "FirstURL" : "http://www.nps.gov/vafo/",
             "Text" : "Official site"
          },
          {
             "Result" : "<a href=\"http://www.nps.gov/vafo/\"><b>Goverment Website</b></a><a href=\"http://www.nps.gov/vafo/\"></a>",
             "Icon" : {
                "URL" : "https://duckduckgo.com/i/nps.gov.ico",
                "Height" : 16,
                "Width" : 16
             },
             "FirstURL" : "http://www.nps.gov/vafo/",
             "Text" : "Official site"
          },{
             "Result" : "<a href=\"http://www.nps.gov/vafo/\"><b>NPS Website site</b></a><a href=\"http://www.nps.gov/vafo/\"></a>",
             "Icon" : {
                "URL" : "https://duckduckgo.com/i/nps.gov.ico",
                "Height" : 16,
                "Width" : 16
             },
             "FirstURL" : "http://www.nps.gov/vafo/",
             "Text" : "Official site"
          },{
             "Result" : "<a href=\"http://www.nps.gov/vafo/\"><b>VAFO NPS Government Website</b></a><a href=\"http://www.nps.gov/vafo/\"></a>",
             "Icon" : {
                "URL" : "https://duckduckgo.com/i/nps.gov.ico",
                "Height" : 16,
                "Width" : 16
             },
             "FirstURL" : "http://www.nps.gov/vafo/",
             "Text" : "Another Official site"
          },
          {
             "Result" : "<a href=\"http://www.nps.gov/vafo/\"><b>Official site</b></a><a href=\"http://www.nps.gov/vafo/\"></a>",
             "Icon" : {
                "URL" : "https://duckduckgo.com/i/nps.gov.ico",
                "Height" : 16,
                "Width" : 16
             },
             "FirstURL" : "http://www.nps.gov/vafo/",
             "Text" : "Official site"
          }
       ],
    "Answer": ""
};

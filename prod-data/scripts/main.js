$(function() {
  var doc = $(document),
      searchForm = $('.search-form');

  getTemplateAjax('templates/search.hbs',searchForm);
  homePageTemplate();
  doc.ajaxComplete(function(){
    BindSearchResults(dataPrebuild);
    //Small hack to reload view
    $('.homePageLogo').click(function(){
      window.location.reload();
    })
  });//ajaxComplete


});//DOM

/*------------------------------------------------------------------
[SEARCH MAIN FUNCATION]
-------------------------------------------------------------------*/

function searchMain(event){
  event.preventDefault();
  searchInput = $('.search-box').filter(':visible:first').val();
  commObj(dataPrebuild);
  return false;
}//searchForm

function homePageTemplate(){
  $('#homePage').show();
  $('#searchResultsPage').hide();
}

function searchResultsTemplate(){
  $('#homePage').hide();
  $('#searchResultsPage').show();

}
/*------------------------------------------------------------------
[BIND RESULTS]
-------------------------------------------------------------------*/
function BindSearchResults(data){
  searchResultsTemplate();
  var template = Handlebars.compile($('#results-template').html());
  Handlebars.registerPartial("RelatedTopics", $("#results-template").html());
  $('#resultsInfo').html(template(data));
    if(!data.RelatedTopics)
      $('#resultsTopic').hide();
}

/*------------------------------------------------------------------
[AJAX - Communication/Templates]
-------------------------------------------------------------------*/

function getTemplateAjax(path,target) {
  var source;
  var template;
  $.ajax({
    url: path, //ex. js/templates/mytemplate.handlebars
    cache: true,
    success: function(data){
      source    = data;
      template  = Handlebars.compile(source);
      target.html(template);
    }
  });
}//getTemplateAjax
